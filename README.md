# Produtos

## Adicionar produto

<iframe width="560" height="315" src="https://www.youtube.com/embed/nNXySoW5GKA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<ul>
    <li>Clique no menu Produtos</li>
    <li>Clique em Adicionar novo</li>
    <li>Preencha o Nome do produto</li>
    <li>Preencha as opções desejadas no box <b>Configurações do produto</b></li>
    <li>No box Dados do produto preencha os seguintes dados:
        <ul>
            <li>Marque Produto Variável</li>
            <li>Na aba Entrega, preencha o peso e as dimensões do produto</li>
        </ul>
    </li>
    <li>Preencha a descrição do produto no box Breve descrição</li>
    <li>Preencha as categorias no box Categorias</li>
    <li>Faça upload da imagem de capa no box Imagem do produto</li>
    <li>Faça upload da galeria de imagens no box Galeria de imagens</li>
    <li>Clique em Publicar</li>
</ul>

## Adicionar opção de Tamanho ao produto

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nmlrx4BMToE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<ul>
    <li>Na aba Atributos:</li>
    <li>Selecione o atributo e clique em adicionar (exemplo: Cor, Tamanho)</li>
    <li>Clique em valores e selecione os valores disponíveis (exemplo: Vermelho, Verde)</li>
    <li>Marque Usado para variações</li>
    <li>Clique em Salvar atributos</li>
    <li>Na aba Variações:</li>
    <li>Selecione Adicionar variação e clique em Ir</li>
    <li><b>Se todos os tamanhos tiverem o mesmo valor:</b><br>Marque o valor "Qualquer Atributo" (exemplo: Qualquer Cor)</li>
    <li><b>Se cada tamanho tiver um valor diferente:</b><br>Siga os passos do vídeo</li>
    <li>Clique no número ao lado dessa opção e selecione o preço</li>
</ul>

## Adicionar opção de Cor ao produto

<iframe width="560" height="315" src="https://www.youtube.com/embed/L7RjqEsQeN0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<ul>
    <li>Clique no menu Produtos</li>
    <li>Clique no sub-menu Product Add-Ons</li>
    <li>No item Cor (branco/marrom) clique em Edit</li>
    <li>No box Products digite o nome do produto que tem a opção de cor</li>
    <li>Clique no nome do produto que aparecer na busca</li>
    <li>Clique em Save group</li>
</ul>

## Adicionar opção de Nomes customizados ao produto

<iframe width="560" height="315" src="https://www.youtube.com/embed/kxTN9d-63kI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<ul>
    <li>Clique no menu Produtos</li>
    <li>Clique no sub-menu Product Add-Ons</li>
    <li>No item Quantidade de Nomes clique em <b>Manage Add-ons</b></li>
    <li>Selecione a quantidade máxima de nomes que terá o tamanho do produto desejado</li>
    <li>No final do campo <b>Variations Requirements</b> insira o nome do produto (exemplo: Papel de Parede)</li>
    <li>Selecione o tamanho correspondente àquela quantidade de nomes (exemplo: 200x150cm)</li>
    <li>Clique no botão azul Save this add-on</li>
</ul>

## Editar produto

<ul>
    <li>Clique no menu Produtos</li>
    <li>Passe o mouse em cima do produto que quer editar</li>
    <li>Clique em Editar</li>
    <li>Altere as informações necessárias</li>
    <li>Clique em Atualizar</li>
</ul>

## Deletar produto

<ul>
    <li>Clique no menu Produtos</li>
    <li>Pase o mouse em cima do produto que quer deletar</li>
    <li>Clique em Colocar na lixeira</li>
</ul>

<br><br>
# Categorias

## Adicionar categoria

<ul>
    <li>Menu Produtos > Categorias</li>
    <li>Preencha o nome da categoria</li>
    <li>Clique em Adicionar nova categoria</li>
</ul>

## Editar categoria

<ul>
    <li>Menu Produtos > Categorias</li>
    <li>Passe o mouse em cima da categoria que deseja editar</li>
    <li>Clique em Editar</li>
    <li>Altere as informações necessárias</li>
    <li>Clique em Atualizar</li>
</ul>

## Excluir categoria

<ul>
    <li>Menu Produtos > Categorias</li>
    <li>Passe o mouse em cima da categoria que deseja excluir</li>
    <li>Clique em Excluir</li>
    <li>Confirme</li>
</ul>

<br><br>
# Páginas

## Editar páginas

<ul>
    <li>Clique no menu <b>Páginas</b></li>
    <li>Clique na página que deseja editar</li>
    <li>No box WPBakery Page Builder passe o mouse em cima do elemento que quer editar e clique no lápis</li>
    <li>Modifique as informações desejadas</li>
    <li>Clique em Atualizar</li>
</ul>

<br><br>
# Banner

## Alterar banner

<ul>
    <li>Clique no menu Banner</li>
    <li>Clique no banner que deseja alterar</li>
    <li>Na aba Main Background clique no botão Media Library e faça upload da imagem de fundo</li>
    <li>Clique em Adicionar camada > Text/HTML para adicionar um botão</li>
    <li>Preencha a nova camada com o texto: <b>&lt;a href="[LINK]" class="slider-btn"&gt;Ver categoria&lt;/a&gt;&lt;/li&gt;</b>
    <li>Clique no disquete verde (Save Slide)</li>
</ul>